﻿using CommonApi.Librairies.GenerateToken;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CommonApi.RestApiHelper
{
    public class RestApiHelper
    {
        public static RestClient RestClient;
        public static RestRequest RestRequest;


        public static RestClient SetUrl(string baseUri, string endpoint)
        {
            var url = Path.Combine(baseUri + endpoint);
            Console.WriteLine(url);
            return RestClient = new RestClient(url);
        }


        public static RestRequest ExecuteHttpRequet<T>(string httpMethode, string Token, T Item)
        {
            httpMethode = httpMethode.Trim().ToLower();
            
            RestRequest restRequet = null;
            switch (httpMethode)
            {
                case "post":
                    RestRequest = new RestRequest(Method.POST);
                    RestRequest.AddHeader("authorization", "Bearer " + Token);
                    RestRequest.AddJsonBody(Item);
                    restRequet = RestRequest;
                    break;
                case "put":
                    RestRequest = new RestRequest(Method.PUT);
                    RestRequest.AddHeader("authorization", "Bearer " + Token);
                    RestRequest.AddJsonBody(Item);
                    restRequet = RestRequest;
                    break;

                case "get":
                    RestRequest = new RestRequest(Method.GET);
                    RestRequest.AddHeader("authorization", "Bearer " + Token);
                    restRequet = RestRequest;
                    break;
                case "delete":
                    RestRequest = new RestRequest(Method.DELETE);
                    RestRequest.AddHeader("authorization", "Bearer " + Token);
                    restRequet = RestRequest;
                    break;

            }
            return restRequet;
        }

        public static IRestResponse GetResponse()
        {
            return RestClient.Execute(RestRequest);
        }

    }
}
