﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderBuilder.StepsDefinitions
{
    public static class TestResults
    {
        public static int StatusCode { get; set; }
        public static string HttpResponseMessageBody { get; set; }

    }
}
