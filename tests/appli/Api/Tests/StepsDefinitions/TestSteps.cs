﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using System.Linq;
using System.Configuration;
using Librairies;

namespace OrderBuilder.StepsDefinitions
{
    [Binding]
    class TestSteps
    {

        public TestSteps()
        {

        }

        [Given(@"I send a ""(.*)"" request on ""(.*)"" to create a new (.*) with the following data")]
        public void GivenISendARequestOnToCreateANewWithTheFollowingData(string httpMethod, string endpoint, string configurationName, Table table)
        {
            var _config = TestHelper.InitConfiguration();
            var baseUri = _config["Apis:test:BaseUri"];
            IEnumerable<dynamic> content = table.CreateDynamicSet().ToList();
            var configuration = new
            {
                name = configurationName + DateManagement.GetCurrentDate().ToString("ddMMyyyyHHmmss"),
                configuration = content 
            };
            Assert.True(true, $"le test est la configuration {configuration.name} et la base uri est {baseUri}");
        }
    }
}
