﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Librairies
{
    public static class DateManagement
    {
        public static DateTime GetCurrentDate()
        {
            TimeZoneInfo info = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(t => t.Id == "Europe/Paris" || t.Id == "Romance Standard Time");
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, info.Id);
        }
    }
}
