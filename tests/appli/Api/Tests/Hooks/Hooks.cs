﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using BoDi;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace OrderBuilder.Hooks
{
    [Binding]
    public class Hooks
        {

        private readonly ScenarioContext senarioContext;

        //Global Variable for Extend report
        private static ExtentTest featureName;
        private static ExtentTest scenario;
        private static AventStack.ExtentReports.ExtentReports extent;
   
        private readonly IObjectContainer _objectContainer;
        private static readonly string PathReport = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\Reports\\"));

        public Hooks(ScenarioContext senarioContext,IObjectContainer objectContainerr)
        {
            _objectContainer = objectContainerr;
            this.senarioContext = senarioContext;
        }

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
           var htmlReporter = new ExtentHtmlReporter(PathReport);
           htmlReporter.Config.Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;
           htmlReporter.Config.ReportName = "appli";
           extent = new AventStack.ExtentReports.ExtentReports();
           //Attach report to reporter
           extent.AttachReporter(htmlReporter);
        }

        [BeforeFeature]
        public static void BeforeFeature(FeatureContext featureContext)
        {
            //Create dynamic feature name
            featureName =  extent.CreateTest<Feature>(featureContext.FeatureInfo.Title);
            Console.WriteLine(featureName);
            Console.WriteLine("BeforeFeature");
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            Console.WriteLine("BeforeScenario" + PathReport);
            scenario = featureName.CreateNode<Scenario>(senarioContext.ScenarioInfo.Title);
        }

        [AfterStep]
        public void InsertReportingSteps()
        {
            var stepType = ScenarioStepContext.Current.StepInfo.StepDefinitionType.ToString();
            if (senarioContext.TestError == null)
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text);
                else if(stepType == "When")
                                scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text);
                else if(stepType == "Then")
                                scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text);
                else if(stepType == "And")
                                scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text);
            }
            else if(senarioContext.TestError != null)
                        {
                if (stepType == "Given")
                {
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Fail(senarioContext.TestError.Message);
                }
                else if(stepType == "When")
                            {
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Fail(senarioContext.TestError.Message);
                }
                else if(stepType == "Then") {
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Fail(senarioContext.TestError.Message);
                }
                else if(stepType == "And")
                            {
                    scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text).Fail(senarioContext.TestError.Message);
                }
            }
        }

        [AfterScenario]
        public void AfterScenario()
        {
            Console.WriteLine("AfterScenario");
        }
        [AfterTestRun]
        public static void AfterTestRun()
        {
            extent.Flush();
        }
}
    }