﻿
using Microsoft.Extensions.Configuration;
using OrderBuilder.StepsDefinitions;
using System;
using System.IO;


namespace OrderBuilder
{
    class TestHelper
    {

        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                     .SetBasePath(Directory.GetCurrentDirectory())
                     .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                     .Build();
            return config;
        }


    }
}
